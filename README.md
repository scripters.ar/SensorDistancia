## Sensor de Distancia por ultrasonido HC-SR04

En este repositorio obtendrás información sobre como conectar y programar un sensor de ultrasonido HC-SR04 con Arduino, para medir distancias entre objetos.

## Materiales

* **Una placa Arduino**
* **Un Sensor Ultrasonido HC-SR04**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo en el IDE de arduino ve a Herramientas --> Monitor Serial
```

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

