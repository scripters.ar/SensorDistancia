int trig = 2;
int echo = 4;
float pulso;
float distancia;

void medirDistancia()
{
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);          //--Enviamos un pulso de 10us
  digitalWrite(trig, LOW);
  
  pulso = pulseIn(echo, HIGH);   //--Obtenemos el ancho del pulso
  distancia = pulso/59;          //--Escalamos el tiempo a una distancia en cm
  
  Serial.print("Distancia: ");
  Serial.print(distancia);      //--Imprimimos el valor del sensor por serial
  Serial.print("cm");
  Serial.println();     
}

void setup()
{
  Serial.begin(9600);
  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
}

void loop()
{
  medirDistancia();
  delay(200);
}
